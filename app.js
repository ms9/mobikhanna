//<debug>
Ext.Loader.setPath({
    'Ext.ux.touch.grid': 'sdk/Ext.ux.touch.grid',
	'Ext': 'sdk/src'
});
//</debug>
/*Ext.Loader.setConfig({
    enabled : true,
    paths   : {
		'Ext': 'sdk/src'
    }
});*/
/*Ext.Loader.setConfig({
    enabled : true,
    paths   : {
        'Ext.ux.touch.grid': 'sdk/Ext.ux.touch.grid',
		'Ext': 'sdk/src'
    }
});*/
Ext.application({
    name: 'venkys_sale',

    requires: [
        'Ext.MessageBox'
    ],

    controllers: ["Feed"],
        
    models: ["Plant","Plantname","Division","Divisionname"],
    stores: ["Plant","Plantname","Division","Divisionname","Poultrydetails","Poultrystock","Cattle","Cattledetails","Cattlestock"],
	views: ["Viewport","MainPanel","Plantpanel","PlantpanelContainer","DivisionPanel","DivisionContainer","PoultryContainer","PoultryPanel","PoultryfeedContainer","PoultrystockContainer","PoultryStock","PoultryDetails","CattleContainer","CattlePanel","CattlefeedContainer","CattleDetails","CattlestockContainer","CattleStock"],
    icon: {
        57: 'resources/icons/Icon.png',
        72: 'resources/icons/Icon~ipad.png',
        114: 'resources/icons/Icon@2x.png',
        144: 'resources/icons/Icon~ipad@2x.png'
    },
    
    phoneStartupScreen: 'resources/loading/Homescreen.jpg',
    tabletStartupScreen: 'resources/loading/Homescreen~ipad.jpg',

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize the main view
        Ext.Viewport.add(Ext.create('venkys_sale.view.Viewport'));
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function() {
                window.location.reload();
            }
        );
    }
});
